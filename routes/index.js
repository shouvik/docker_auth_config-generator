var express = require("express");
var router = express.Router({mergeParams:true});

router.get("/", function(req, res) {
    res.render("index");
});

router.get("/generic", function(req, res) {
    res.render("generic");
});

router.get("/elements", function(req, res) {
    res.render("elements");
});

module.exports = router;