var express = require("express"),
    app = express()

var  indexRoutes = require("./routes/index");
var port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080,
    ip   = process.env.IP   || process.env.OPENSHIFT_NODEJS_IP || '0.0.0.0';

app.use(express.static(__dirname + "/public"));
app.set("view engine", "ejs");

//ROUTES
app.use(indexRoutes);


app.listen(port, ip, () =>{
    console.log("App running at %s:%s",ip,port);
});
